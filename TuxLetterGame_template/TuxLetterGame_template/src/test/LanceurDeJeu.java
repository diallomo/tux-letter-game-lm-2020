/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import game.Jeu;
import game.JeuDevineLeMotOrdre;
import java.io.IOException;
import org.xml.sax.SAXException;

/**
 *
 * @author Mohamed
 */
public class LanceurDeJeu {
    public static void main(String[] args) throws IOException, SAXException{
                // Declare un Jeu
        Jeu j;
        //Instancie un nouveau jeu
        j = new JeuDevineLeMotOrdre();
        //Execute le jeu
        j.execute();
        
    }

    
}
