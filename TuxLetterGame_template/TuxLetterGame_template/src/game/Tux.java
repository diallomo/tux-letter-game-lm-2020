/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import env3d.Env;
import env3d.advanced.EnvNode;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author Mohamed
 */
public class Tux extends EnvNode {
    private final Env env;
    private final Room room;
    
    public Tux(Env env, Room room){
        this.env = env;
        this.room = room;
        
        setScale(4.0);
        setX( this.room.getWidth()/2 );     // positionnement au milieu de la largeur de la room
        setY(getScale() * 1.1);             // positionnement en hauteur basé sur la taille de Tux
        setZ( this.room.getDepth()/2 );     // positionnement au milieu de la profondeur de la room
        setTexture("models/tux/tux_special.png");
        setModel("models/tux/tux.obj"); 
        
    }
    
       public Boolean testeRoomCollision(double x, double z){
        Boolean bool = true;
        
        if((x <= 0.0) || (z <= 0.0) || (this.room.getWidth() <= x) || (this.room.getDepth() <= z)  ){
            bool = false;
        }
        
        return bool;
        }   
    
     
     
    public void deplacer_tux(){
        if (env.getKeyDown(Keyboard.KEY_UP)) { 
            this.setRotateY(180);
            if(testeRoomCollision(this.getX(), this.getZ()-1)){
                this.setZ(this.getZ() - 1.0);
            }
                 
        }

        
        if (env.getKeyDown(Keyboard.KEY_LEFT)) { 
            this.setRotateY(270);
            if(testeRoomCollision(this.getX()-1, this.getZ())){
                this.setX(this.getX() - 1.0);
            }
        }
        
        if (env.getKeyDown(Keyboard.KEY_RIGHT)) { 
            this.setRotateY(90);
            if(testeRoomCollision(this.getX()+1, this.getZ())){
                this.setX(this.getX() + 1.0);
            }
            
        }
        
        if (env.getKeyDown(Keyboard.KEY_DOWN)) { 
            this.setRotateY(360);
            if(testeRoomCollision(this.getX(), this.getZ()+1)){
                this.setZ(this.getZ() + 1.0);
            }
        }
        
    }
    
}
