/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import env3d.advanced.EnvNode;

/**
 *
 * @author farid
 */
public class Letter extends EnvNode {
    
    protected char letter;
    private double x;
    private double y;
    private double z;

    public char getLetter() {
        return letter;
    }


    
    
    // J'ai modifié les parametres de Letter car comme nous somme deja dans une room,
    // forcement on autra les coordonnées x et y, 
    // et utiliser une room en parametre c'est plus rapide 
    public Letter(char l, Room room, Tux tux){
        
        this.letter = l;

        // Affichage d'un cube sans lettre si un espace est donné en paramètre du constructeur
        String lePng = "cube.png";
        if (l != ' '){
            lePng = l + ".png";
        }
   
        x = Math.random() * room.getDepth() ;
        y = tux.getScale() * 1.1 ; 
        z = Math.random() * room.getWidth() ;
        
        
        setScale(4.0);
        setX(x);
        setY(y); 
        setZ(z); 
        setTexture("models/letter/" + lePng);
        setModel("models/letter/cube.obj");  

        
        
        
    }

   
}
    
    