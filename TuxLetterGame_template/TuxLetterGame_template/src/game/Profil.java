/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.File;
import java.util.ArrayList;
import org.w3c.dom.Document;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import utilitaire.XMLUtil ;



/**
 *
 * @author farid
 */
public class Profil {
    public Document doc;
    
    private String nom;
    private String dateNaissance;
    private ArrayList<Partie> listeParties;
    protected ArrayList<Partie> listeParties_NON;
    

    public Profil(String nom, String dateNaissance) {
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.listeParties = new ArrayList<Partie>();
        this.listeParties_NON = new ArrayList<Partie>(); 
    }

    Profil(String nomFichier) {
        this.doc = fromXML(nomFichier);
        this.listeParties = new ArrayList<Partie>();
        this.listeParties_NON = new ArrayList<Partie>(); 

        this.nom = doc.getElementsByTagName("nom").item(0).getTextContent();
        this.dateNaissance = doc.getElementsByTagName("anniversaire").item(0).getTextContent();
        
        int l = doc.getElementsByTagName("partie").getLength();
        int i = 0;

        Partie p;
        while(i < l)
        {
            p = new Partie((Element)doc.getElementsByTagName("partie").item(i));
            this.listeParties.add(p);
            if(p.getTrouve() < 100){
                
                this.listeParties_NON.add(p);
            }
            
            i++;
        }
        
    }
 
    public void ajouterPartie(Partie p){
        this.listeParties.add(p);
    }

    private int getDernierNiveau(){
        return listeParties.get(listeParties.size() - 1).getNiveau();
        
    }
    
    @Override
    public String toString(){
        return "Les parties du profil sont les suivantes : " + listeParties.size();
        
    }

    public String getNom() {
        return nom;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }
    
    public void sauvegarder() throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
        try
        {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance() ;
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            
            // element racine
            Document d = docBuilder.newDocument() ;
            Element profil = d.createElement("profil");
            d.appendChild(profil);
            
            // nom
            Element nom = d.createElement("nom");
            nom.appendChild( d.createTextNode(this.nom) );
            profil.appendChild(nom);
            
            // date de naissance
            Element anniv = d.createElement("anniversaire");
            anniv.appendChild( d.createTextNode(this.dateNaissance) );
            profil.appendChild(anniv);
            
            // parties
            Element lPartie = d.createElement("parties");
            profil.appendChild(lPartie);
            
            // partie
            for(Partie p : this.listeParties)
            {
                Element partie = d.createElement("partie");
                lPartie.appendChild(partie);
                
                // attribut date
                Attr a = d.createAttribute("date");
                a.setValue(p.getDate());
                partie.setAttributeNode(a);
                
                String tmp = "";
                tmp += p.getTemps() ;
                
                // temps
                Element temps = d.createElement("temps");
                temps.appendChild(d.createTextNode(tmp));
                partie.appendChild(temps);
                
                // mot
                Element mot = d.createElement("mot");
                mot.appendChild(d.createTextNode(p.getMot()));
                partie.appendChild(mot);
                
                // attribut niveau
                Attr a2 = d.createAttribute("niveau");
                tmp = "";
                tmp += p.getNiveau();
                a2.setValue(tmp);
                mot.setAttributeNode(a2);
                
                // mot
                Element mot_fini = d.createElement("mot_fini");
                mot_fini.appendChild(d.createTextNode(p.getMot_fini()));
                partie.appendChild(mot_fini);
                
                tmp = "";
                tmp += p.getTrouve();
                // trouve
                Element trouve = d.createElement("trouve");
                trouve.appendChild(d.createTextNode(tmp));
                partie.appendChild(trouve); 
            }
            
            // mettre le contenu dans un XML
            
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(d);
            StreamResult result = new StreamResult(new File("src/xml/profil/" + this.nom + ".xml"));
            
            // Test console
            StreamResult result2 = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println("File saved!");
        } catch (ParserConfigurationException pce) {
        pce.printStackTrace();
      } catch (TransformerException tfe) {
        tfe.printStackTrace();
      }
        
    } 
    
    // Cree un DOM à partir d'un fichier XML
    public Document fromXML(String nomFichier) {
        try {
            return XMLUtil.DocumentFactory.fromFile(nomFichier);
        } catch (Exception ex) {
            //Logger.getLogger(Profil.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Zwa");
        }
        return null;
    }

    // Sauvegarde un DOM en XML
    public void toXML(String nomFichier) {
        try {
            XMLUtil.DocumentTransform.writeDoc(this.doc, nomFichier);
        } catch (Exception ex) {
            Logger.getLogger(Profil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /// Takes a date in XML format (i.e. ????-??-??) and returns a date
    /// in profile format: dd/mm/yyyy
    public static String xmlDateToProfileDate(String xmlDate) {
        String date;
        // récupérer le jour
        date = xmlDate.substring(xmlDate.lastIndexOf("-") + 1, xmlDate.length());
        date += "/";
        // récupérer le mois
        date += xmlDate.substring(xmlDate.indexOf("-") + 1, xmlDate.lastIndexOf("-"));
        date += "/";
        // récupérer l'année
        date += xmlDate.substring(0, xmlDate.indexOf("-"));

        return date;
    }

    /// Takes a date in profile format: dd/mm/yyyy and returns a date
    /// in XML format (i.e. ????-??-??)
    public static String profileDateToXmlDate(String profileDate) {
        String date;
        // Récupérer l'année
        date = profileDate.substring(profileDate.lastIndexOf("/") + 1, profileDate.length());
        date += "-";
        // Récupérer  le mois
        date += profileDate.substring(profileDate.indexOf("/") + 1, profileDate.lastIndexOf("/"));
        date += "-";
        // Récupérer le jour
        date += profileDate.substring(0, profileDate.indexOf("/"));

        return date;
    }
   
    
}
