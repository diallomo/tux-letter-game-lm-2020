/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import org.w3c.dom.Document;
import utilitaire.XMLUtil ;

/**
 *
 * @author farid
 */
public class Room {
    private int depth;
    private int height;
    private int width;
    private String textureBottom;
    private String textureNorth;
    private String textureEast;
    private String textureWest;
    private String textureTop;
    private String textureSouth;
    
    // Constructeur par defaut de la room
    /*
    public Room(){
        // initialisation des attributs à leur valeur par défaut
        this.depth = 100;
        this.height = 60;
        this.width = 100;
        
        this.textureBottom = "/textures/skybox/city/bottom.png";
        this.textureNorth = "/textures/skybox/city/north.png";
        this.textureEast = "/textures/skybox/city/east.png";
        this.textureWest = "/textures/skybox/city/west.png";
        this.textureTop = null;
        this.textureSouth = null;
        
    }
*/
    
    public Room(String chemin){
        Document doc = fromXML(chemin);
        
        this.depth = Integer.parseInt(doc.getElementsByTagName("depth").item(0).getTextContent());
        this.height = Integer.parseInt(doc.getElementsByTagName("height").item(0).getTextContent());
        this.width = Integer.parseInt(doc.getElementsByTagName("width").item(0).getTextContent());
        
        this.textureBottom = doc.getElementsByTagName("textureBottom").item(0).getTextContent();
        this.textureNorth = doc.getElementsByTagName("textureNorth").item(0).getTextContent();
        this.textureEast = doc.getElementsByTagName("textureEast").item(0).getTextContent();
        this.textureWest = doc.getElementsByTagName("textureWest").item(0).getTextContent();
        this.textureTop = null;
        this.textureSouth = null;
        
    }
    
     // Cree un DOM à partir d'un fichier XML
    public Document fromXML(String nomFichier) {
        try {
            return XMLUtil.DocumentFactory.fromFile(nomFichier);
        } catch (Exception ex) {
            //Logger.getLogger(Profil.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Zwa");
        }
        return null;
    }
    
    // Ajout des getters 
    public int getDepth() {
        return depth;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public String getTextureBottom() {
        return textureBottom;
    }

    public String getTextureNorth() {
        return textureNorth;
    }

    public String getTextureEast() {
        return textureEast;
    }

    public String getTextureWest() {
        return textureWest;
    }

    public String getTextureTop() {
        return textureTop;
    }

    public String getTextureSouth() {
        return textureSouth;
    }
    
    // Ajout des Setters 
    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setTextureBottom(String textureBottom) {
        this.textureBottom = textureBottom;
    }

    public void setTextureNorth(String textureNorth) {
        this.textureNorth = textureNorth;
    }

    public void setTextureEast(String textureEast) {
        this.textureEast = textureEast;
    }

    public void setTextureWest(String textureWest) {
        this.textureWest = textureWest;
    }

    public void setTextureTop(String textureTop) {
        this.textureTop = textureTop;
    }

    public void setTextureSouth(String textureSouth) {
        this.textureSouth = textureSouth;
    }
    
    
}