/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import env3d.Env;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.lwjgl.input.Keyboard;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import utilitaire.LectureClavier ;
import utilitaire.XMLUtil;
import utilitaire.XMLUtil.DocumentFactory;
import static utilitaire.XMLUtil.DocumentTransform.fromDefaultTransformation;

/**
 *
 * @author diallomo et mezianef
 */
public abstract class Jeu {

    enum MENU_VAL {
        MENU_SORTIE, MENU_CONTINUE, MENU_JOUE
    }

    private final Room menuRoom;
    private Profil profil;
    private final Dico dico;
    
    protected final Room mainRoom;
    protected final Env env;
    protected Tux tux;
    protected EnvTextMap menuText;                         //text (affichage des texte du jeu)
    protected ArrayList<Letter> lettres;
    protected boolean finished;

    
    public Jeu() throws IOException, SAXException {

        // Crée un nouvel environnement
        env = new Env();
        
        String cheminRoom = "src/xml/room.xml";
        

        // Instancie une Room
        mainRoom = new Room(cheminRoom);

        // Instancie une autre Room pour les menus
        menuRoom = new Room(cheminRoom);
        menuRoom.setTextureEast("textures/black.png");
        menuRoom.setTextureWest("textures/black.png");
        menuRoom.setTextureNorth("textures/black.png");
        menuRoom.setTextureBottom("textures/black.png");

        // Règle la camera
        env.setCameraXYZ(50, 60, 175);
        env.setCameraPitch(-20);

        // Désactive les contrôles par défaut
        env.setDefaultControl(false);


        
        // On instancie un dictionnaire et on lit les données depuis le XML
        dico = new Dico("src/xml/");
        dico.lireDictionnaireDOM(dico.getChememinFichierDico(), "dico.xml");
        
        // instancie le menuText
        menuText = new EnvTextMap(env);
        
        // Textes affichés à l'écran
        menuText.addText("Voulez vous ?", "Question", 200, 300);
        menuText.addText("1. Commencer une nouvelle partie ?", "Jeu1", 250, 280);
        menuText.addText("2. Charger une partie existante ?", "Jeu2", 250, 260);
        menuText.addText("3. Sortir de ce jeu ?", "Jeu3", 250, 240);
        menuText.addText("4. Quitter le jeu ?", "Jeu4", 250, 220);
        menuText.addText("Choisissez un nom de joueur : ", "NomJoueur", 200, 300);
        menuText.addText("1. Charger un profil de joueur existant ?", "Principal1", 250, 280);
        menuText.addText("2. Créer un nouveau joueur ?", "Principal2", 250, 260);
        menuText.addText("3. Quitter le jeu ?", "Principal3", 250, 240);
        
        menuText.addText("Veuillez choisir un numero de partie : ", "Partie", 50, 450);
        menuText.addText("Joueur inexistant", "EXIT", 50, 450);
        
        menuText.addText("Nouvelle partie niveau 1 - 5 : ", "Niveau", 200, 300);
        menuText.addText("Saisir une date de naissance JJ-MM-YYYY : ", "Naissance", 200, 300);
 
    }

    /**
     * Gère le menu principal
     *
     */
    public void execute() throws JAXBException, ParserConfigurationException, SAXException, IOException, TransformerException {

        MENU_VAL mainLoop;
        mainLoop = MENU_VAL.MENU_SORTIE;
        do {
            mainLoop = menuPrincipal();
        } while (mainLoop != MENU_VAL.MENU_SORTIE);
        this.env.setDisplayStr("Au revoir !", 300, 30);
        env.exit();
    }


    // fourni
    private String getNomJoueur() {
        String nomJoueur = "";
        menuText.getText("NomJoueur").display();
        nomJoueur = menuText.getText("NomJoueur").lire(true);
        menuText.getText("NomJoueur").clean();
        return nomJoueur;
    }
    
    //Cette methode permet de récupérer le niveau    
    private String getNiveau() {
        String niveau = "";
        menuText.getText("Niveau").display();
        niveau = menuText.getText("Niveau").lire(true);
        menuText.getText("Niveau").clean();
        return niveau ;
        
    }
    
   //Cette methode permet de récupérer la date de naissance pour les nouveaux profils    
    private String getNaissance() {
        String naissance = "";
        menuText.getText("Naissance").display();
        naissance = menuText.getText("Naissance").lire(true);
        menuText.getText("Naissance").clean();
        return naissance ;
        
    }  
    
    private String getPartie() {
        String partie = "";
        menuText.getText("Partie").display();
        partie = menuText.getText("Partie").lire(true);
        menuText.getText("Partie").clean();
        return partie ;
        
    } 
 
    
    // fourni, à compléter
    private MENU_VAL menuJeu() throws JAXBException, ParserConfigurationException, SAXException, IOException, TransformerException { // MENU2

        MENU_VAL playTheGame;
        playTheGame = MENU_VAL.MENU_JOUE;
        Partie partie;
        
        do {
            // restaure la room du menu
            env.setRoom(menuRoom);
            
            // affiche menu
            menuText.getText("Question").display();
            menuText.getText("Jeu1").display();
            menuText.getText("Jeu2").display();
            menuText.getText("Jeu3").display();
           
            
            // vérifie qu'une touche 1, 2, 3 ou 4 est pressée
            int touche = 0;
            while (!(touche == Keyboard.KEY_1 || touche == Keyboard.KEY_2 || touche == Keyboard.KEY_3)) {
                touche = env.getKey();
                env.advanceOneFrame();
            }

            // nettoie l'environnement du texte
            menuText.getText("Question").clean();
            menuText.getText("Jeu1").clean();
            menuText.getText("Jeu2").clean();
            menuText.getText("Jeu3").clean();
            

            if(touche == Keyboard.KEY_2 && this.profil.listeParties_NON.isEmpty()){
                touche = Keyboard.KEY_1;
            }

            // et décide quoi faire en fonction de la touche pressée
            switch (touche) {
                // -----------------------------------------
                // Touche 1 : Commencer une nouvelle partie
                // -----------------------------------------                
                case Keyboard.KEY_1: 

                    // choisi un niveau et un mot
                    int niveau = Integer.parseInt(getNiveau());
                    String mot = dico.getMotDepuisListeNiveaux(niveau);

                    // crée un nouvelle partie
                    partie = new Partie(aujourdhui(), mot, niveau);
                    
                    // joue
                    joue(partie);
                    
                    // enregistre la partie dans le profil --> enregistre le profil
                    this.profil.ajouterPartie(partie);
                    
                    // .......... profil.******
                    playTheGame = MENU_VAL.MENU_JOUE;
                    
                    break;

                // -----------------------------------------
                // Touche 2 : Charger une partie existante
                // -----------------------------------------                
                case Keyboard.KEY_2: // charge une partie existante
                    
                    int i = 0;
                    int y = 0;
                    for (Partie p : this.profil.listeParties_NON) {
                        
                        menuText.addText((i+1) + ".\t" + p.toString(), "Partie"+i, 50, 400-y);
                        i++;
                        y += 40;
                    } 
                    
                    i = 0;
                    // affichage des parties
                    for (Partie p : this.profil.listeParties_NON) {
                        
                        menuText.getText("Partie"+i).display();
                        i++;
                    } 
                    
                    // Choix de la partie
                    int np = Integer.parseInt(getPartie()) - 1;
                    
                    
                    i = 0;
                    // nettoyage de l'ecran
                    for (Partie p : this.profil.listeParties_NON) {
                        
                        menuText.getText("Partie"+i).clean();
                        i++;
                    } 
                    
                    partie = this.profil.listeParties_NON.get(np);
                    
                    // On supprime la partie de la liste des parties
                    this.profil.listeParties_NON.remove(np);
                 
                    joue(partie);
                    
                    // On rajoute la partie dans la liste des parties
                    
                    this.profil.ajouterPartie(partie);
                    
                    playTheGame = MENU_VAL.MENU_JOUE;
                    break;

                // -----------------------------------------
                // Touche 3 : Sortie de ce jeu
                // -----------------------------------------                
                case Keyboard.KEY_3:
                    playTheGame = MENU_VAL.MENU_CONTINUE;
                    profil.sauvegarder();   
                    
                    break;

                
                   
            }
        } while (playTheGame == MENU_VAL.MENU_JOUE);
        
        return playTheGame;
    }

    private MENU_VAL menuPrincipal() throws JAXBException, ParserConfigurationException, SAXException, IOException, TransformerException { // MENU1

        MENU_VAL choix = MENU_VAL.MENU_CONTINUE;
        String nomJoueur;
        String naissance;

        // restaure la room du menu
        env.setRoom(menuRoom);

        menuText.getText("Question").display();
        menuText.getText("Principal1").display();
        menuText.getText("Principal2").display();
        menuText.getText("Principal3").display();
               
        // vérifie qu'une touche 1, 2 ou 3 est pressée
        int touche = 0;
        while (!(touche == Keyboard.KEY_1 || touche == Keyboard.KEY_2 || touche == Keyboard.KEY_3)) {
            touche = env.getKey();
            env.advanceOneFrame();
        }

        // Nettoie la le menu
        menuText.getText("Question").clean();
        menuText.getText("Principal1").clean();
        menuText.getText("Principal2").clean();
        menuText.getText("Principal3").clean();

        // et décide quoi faire en fonction de la touche pressée
        switch (touche) {
            // -------------------------------------
            // Touche 1 : Charger un profil existant
            // -------------------------------------

            case Keyboard.KEY_1:
                // demande le nom du joueur existant
                nomJoueur = getNomJoueur();
                String chemin = "src/xml/profil/" + nomJoueur + ".xml" ;
    
                
                
                // charge le profil de ce joueur si possible               
                if (profil_existe(chemin)) {
                    profil = new Profil(chemin);
                    choix = menuJeu();
                } 
                else {
                    choix = MENU_VAL.MENU_SORTIE;//CONTINUE;
                }
                break;

            // -------------------------------------
            // Touche 2 : Créer un nouveau joueur
            // -------------------------------------
            case Keyboard.KEY_2:
                // nom et date de naissance du nouveau joueur
                nomJoueur = getNomJoueur();
                naissance = getNaissance();
                
                // crée un profil avec le nom d'un nouveau joueur
                profil = new Profil(nomJoueur, naissance);
                choix = menuJeu();
                break;

            // -------------------------------------
            // Touche 3 : Sortir du jeu
            // -------------------------------------
            case Keyboard.KEY_3:
                choix = MENU_VAL.MENU_SORTIE;
        }
        return choix;
    }

    public void joue(Partie partie) {
        this.finished = false;
 
        menuText.addText("Voulez vous voir le mot avant de commencer ? (O/N) ", "facile", 200, 300);
        menuText.getText("facile").display();
        String rep = menuText.getText("facile").lire(true);
        menuText.getText("facile").clean();
        
        if(rep.equals("O"))
        {
            menuText.addText("Touve ce mot : " + partie.getMot() + "\nOK pour continuer : ", "mot", 200, 300);
            menuText.getText("mot").display();
            String z = menuText.getText("mot").lire(true);
            menuText.getText("mot").clean();
        }
        
        
        // restaure la room du jeu
        env.setRoom(mainRoom);
        
        
        // Instancie un Tux
        tux = new Tux(env, mainRoom);
        env.addObject(tux);

        //letter = new Letter('a', 10, 10);
        //env.addObject(letter);

        // Ici, on peut initialiser des valeurs pour une nouvelle partie
        demarrePartie(partie);

        // Boucle de jeu
        Boolean win;
        win = false;
        
        while (!this.finished) {
            // On vérifie quelle touche a pressé le joueur et on déplace Tux
            checkInput();
            
            // On applique les règles de jeu
            win = appliqueRegles(partie);
 
        }

        // Ici on peut calculer des valeurs lorsque la partie est terminée
        terminePartie(partie, win);

    }
    
    protected void checkInput(){
        int input ; 
        
        // On recupere la touche clavier
        input = this.env.getKey() ;
        
        // Mise a jour de l'affichage
        this.env.advanceOneFrame();
        
        // On fait bouger Tux 
        this.tux.deplacer_tux();
        
        // On utilise la touche q pour sortir du jeu
        if(input == Keyboard.KEY_Q){
            this.finished = true;
            System.out.println("Quitter : fin de la partie");
        }   
    }
    
    protected double distance(Letter letter) {
        double distance;

        distance = Math.sqrt(((this.tux.getX()-letter.getX())*(this.tux.getX()-letter.getX()))+((this.tux.getZ()-letter.getZ())*(this.tux.getZ()-letter.getZ())));
             return distance;
           
    }
        
    protected boolean collision(Letter letter){
        
        boolean col = false;
   
        if(distance(letter) < 7){

            col = true ;
        }
        
        return col;
    }
    
    public boolean profil_existe(String chemin){
        boolean existe = true;
        
        try {
            XMLUtil.DocumentFactory.fromFile(chemin);
        } catch (Exception ex) {
            existe = false;
        }
        
        return existe;
    }
    
    public String aujourdhui() {
     return LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    protected abstract void demarrePartie(Partie partie);

    protected abstract boolean appliqueRegles(Partie partie);

    protected abstract void terminePartie(Partie partie, boolean win);

    
    
}
