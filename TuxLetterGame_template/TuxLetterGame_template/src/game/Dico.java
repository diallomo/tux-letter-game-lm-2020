/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Mohamed
 */
public class Dico {
    private ArrayList<String> listeNiveau1;
    private ArrayList<String> listeNiveau2;
    private ArrayList<String> listeNiveau3;
    private ArrayList<String> listeNiveau4;
    private ArrayList<String> listeNiveau5;

    private String cheminFichierDico;
    
        
    Random rand;
    
    public Dico(String cheminFichierDico) {
        this.cheminFichierDico = cheminFichierDico;
        
        listeNiveau1 = new ArrayList<String>();
        listeNiveau2 = new ArrayList<String>();
        listeNiveau3 = new ArrayList<String>();
        listeNiveau4 = new ArrayList<String>();
        listeNiveau5 = new ArrayList<String>();
        
    }
     // méthode qui me permet d'extraire un mot au hasard depuis une arraylist
    private String getMotDepuisListe(ArrayList<String> list){
        double index_double ;
        int index ;
        String motExtrait ;
        
        if (list.size() > 0){
            index_double = Math.random() * list.size() ;
            index = (int)index_double;
            motExtrait = list.get(index);
        }
        else {
            // Mot par défaut au cas ou la liste est vide 
            motExtrait = "football";
        }
       
        return motExtrait;
    }   
    
    
    // méthode qui me permet de vérifier le niveau 
    private int verifieNiveau(int niveau){
        int nv = 0;
        switch(niveau){
            case 1 : 
                nv = 1;
                break;
            case 2 : 
                nv = 2;
                break;
            case 3 :
                nv = 3;
                break;
            case 4 : 
                nv = 4;
                break;
            case 5 :
                nv = 5;
                break;
                
            default :
                nv = 1;
                break;
        }
        return nv;
    }

    // méthode qui permet de piocher un mot de la liste dont le niveau est passer en parametre
    public String getMotDepuisListeNiveaux(int niveau){
        
        
        String niveauMot = "";
        switch(verifieNiveau(niveau)){
            case 1 :
                if (this.listeNiveau1.size() > 0){
                    niveauMot = getMotDepuisListe(this.listeNiveau1);
                }
                break;
            case 2 :
                if (this.listeNiveau2.size() > 0){
                    niveauMot = getMotDepuisListe(this.listeNiveau2);
                }
                break;
            case 3 :
                if (this.listeNiveau3.size() > 0){
                    niveauMot = getMotDepuisListe(this.listeNiveau3);
                }
                break;
            case 4 :
                if (this.listeNiveau4.size() > 0){
                    niveauMot = getMotDepuisListe(this.listeNiveau4);
                }
                break;
            case 5 :
                if (this.listeNiveau5.size() > 0){
                    niveauMot = getMotDepuisListe(this.listeNiveau5);
                }
                break;
            
        }
        return niveauMot;
    }

    // méthode qui permet d'ajouter un mot dans la liste en fonction de son niveau 
    public void ajouteMotADico(int niveau, String mot){
        
        switch(verifieNiveau(niveau)){
            case 1 : 
                this.listeNiveau1.add(mot);
                break;
            case 2 : 
                this.listeNiveau2.add(mot);
                break;
            case 3 : 
                this.listeNiveau3.add(mot);
                break;
            case 4 : 
                this.listeNiveau4.add(mot);
                break;
            case 5 : 
                this.listeNiveau5.add(mot);
                break;
        }
        
    }    
    
    public String getChememinFichierDico(){
        return cheminFichierDico;
    }
    
    public void lireDictionnaireDOM(String path, String filename) throws IOException, SAXException{
        
        // crée un parser de type DOM
        DOMParser parser = new DOMParser();
        // parse le document XML correspondant au fichier filename dans le chemin path
        parser.parse(path + filename);
        // récupère l'instance de document
        Document doc = parser.getDocument();
    
        // Je commnence par récupérer les listes de mots 
        NodeList TlisteMots;
        TlisteMots = doc.getElementsByTagName("listemots");

        // Je parcoures c'est listes de mots 
        for(int i=0; i<TlisteMots.getLength();i++){
            // Je récupére le mot d'indice i 
            NodeList motsNVi = ((Element) TlisteMots.item(i)).getElementsByTagName("mot");
            
            // Je récupére le niveau du mot d'indice i
            int nv = Integer.parseInt(((Element) TlisteMots.item(i)).getAttribute("niveau"));
           
            switch(verifieNiveau(nv)){
                case 1 : 
                    // J'ittere sur chaque mot de niveau i pour aisni récupérer le mot de niveau i 
                    for(int j=0; j<motsNVi.getLength();j++){
                        Element mots1 = (Element) motsNVi.item(j);
                        String mots1Rec = mots1.getTextContent();
                        ajouteMotADico(nv, mots1Rec);
                        //System.out.println("Mot : "+ mots1Rec + " --> niveau : "+nv);
                    }

                    break;
                    
                case 2 : 
                    for(int j=0; j<motsNVi.getLength();j++){
                        Element mots2 = (Element) motsNVi.item(j);
                        String mots2Rec = mots2.getTextContent();
                        ajouteMotADico(nv, mots2Rec);
                        //System.out.println("Mot : "+ mots2Rec + " --> niveau : "+nv);
                    }
                    break;
                    
                case 3 : 
                    for(int j=0; j<motsNVi.getLength();j++){
                        Element mots3 = (Element) motsNVi.item(j);
                        String mots3Rec = mots3.getTextContent();
                        ajouteMotADico(nv, mots3Rec);
                        //System.out.println("Mot : "+ mots3Rec + " --> niveau : "+nv);
                    }
                    break; 
                    
                case 4 : 
                    for(int j=0; j<motsNVi.getLength();j++){
                        Element mots4 = (Element) motsNVi.item(j);
                        String mots4Rec = mots4.getTextContent();
                        ajouteMotADico(nv, mots4Rec);
                        //System.out.println("Mot : "+ mots4Rec + " --> niveau : "+nv);
                    }
                    break;
                    
                case 5 : 
                    for(int j=0; j<motsNVi.getLength();j++){
                        Element mots5 = (Element) motsNVi.item(j);
                        String mots5Rec = mots5.getTextContent();
                        ajouteMotADico(nv, mots5Rec);
                        //System.out.println("Mot : "+ mots5Rec + " --> niveau : "+nv);
                    }
                    break;
            }
            
            
        }
    }
}
