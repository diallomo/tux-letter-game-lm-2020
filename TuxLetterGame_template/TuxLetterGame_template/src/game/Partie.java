/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author farid
 */
public class Partie {
    private String date ;
    private String mot ;
    private String mot_fini ;
    private int niveau ;
    private int trouve ;
    private int temps ;

    public Partie(String date, String mot, int niveau) {
        this.date = date;
        this.mot = mot;
        this.mot_fini = mot;
        this.niveau = niveau;
        this.trouve = 0;
        this.temps = 0;
    }
    
    public Partie (Element partieElt){
        //this.date = partieElt.getElementsByTagName("date").item(0).getNodeValue();
        
        this.date = partieElt.getAttribute("date");
        this.mot = partieElt.getElementsByTagName("mot").item(0).getTextContent();
        this.mot_fini = partieElt.getElementsByTagName("mot_fini").item(0).getTextContent();
        
        Element m = (Element)partieElt.getElementsByTagName("mot").item(0);
        
        this.niveau = Integer.parseInt( m.getAttribute("niveau") ) ;
        
        this.trouve = Integer.parseInt(partieElt.getElementsByTagName("trouve").item(0).getTextContent());
        this.temps = Integer.parseInt(partieElt.getElementsByTagName("temps").item(0).getTextContent());
        
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public String getDate() {
        return date;
    }
    
    

    
    public int getTrouve() {
        return trouve;
    }

    public String getMot() {
        return mot;
    }

    public String getMot_fini() {
        return mot_fini;
    }
    
    
    
    public void setTrouve(int nbLettresRestantes){
        float formule;
        float l = mot_fini.length() ;
        formule = (l - nbLettresRestantes) / l ;
        
        float a = formule * 100 ;
        
        trouve = (int)a;
      
    }
    
    public void setTemps(int temps){
        this.temps = temps ;
    }
    
    public int getNiveau(){
        return this.niveau ;
    }

    
    
    public int getTemps() {
        return temps;
    }
     
    @Override
    public String toString(){
        String affichage = "Date : " + this.date + "\tNiveau : " + this.niveau + "\tPourcentage trouve : " + this.trouve;
        return affichage ;
    }
    
   
    public Element getPartie(Document doc) throws ParserConfigurationException, SAXException, IOException{
     
        // Cette méthode ne nous servira pas car elle a déjà été implémenter dans sauvegarde
        
        return null;
        
    }
    
    
}