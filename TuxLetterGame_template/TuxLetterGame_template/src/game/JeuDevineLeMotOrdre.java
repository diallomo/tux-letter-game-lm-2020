/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.IOException;
import java.util.ArrayList;
import org.xml.sax.SAXException;

/**
 *
 * @author Mohamed & Farid
 */

public class JeuDevineLeMotOrdre extends Jeu{
    private int nbLettresRestantes ;
    private Chronometre chrono ;
    
    public JeuDevineLeMotOrdre() throws IOException, SAXException {
        super() ;  
        chrono = new Chronometre(20) ;
        // On instancie l'ArrayList
        lettres = new ArrayList<Letter>();
        

    }

    public void setNbLettresRestantes(int nbLettresRestantes) {
        this.nbLettresRestantes = nbLettresRestantes;
    }

    @Override
    protected void demarrePartie(Partie partie) {
        setNbLettresRestantes(partie.getMot().length()) ;
        
        // Ajout des lettres du mot a l'ArrayList lettres
        Letter le ;
        
        for(int i = 0; i<partie.getMot().length(); i++){
            le = new Letter(partie.getMot().charAt(i), mainRoom, tux);
            this.lettres.add(le);
        }
            
        // Ajout des lettres a l'env
         for(Letter l : lettres){
            env.addObject(l); 
        }
         
        chrono.start();
  
    }


    @Override
    protected boolean appliqueRegles(Partie partie) {
        boolean win = false ;
        String tmp = "";
 
        if(chrono.remainsTime() > 0){
            if( tuxTrouveLettre() ){
                this.nbLettresRestantes --;
      
                tmp = partie.getMot().substring(partie.getMot().length() - this.nbLettresRestantes);
                
                partie.setMot(tmp);
            }

            if(nbLettresRestantes == 0){               
                this.finished = true;
                win = true;
                partie.setMot(partie.getMot_fini());  
            }           
        }
        else{
            this.finished = true ;
        }
        
        return win;
     
    }

    @Override
    protected void terminePartie(Partie partie, boolean win) {
        chrono.stop();
        if(win){
           partie.setTemps((int) chrono.getSeconds())  ; 
           System.out.println("Temps de jeu : " + partie.getTemps() + " sec");
        }
    
        partie.setTrouve(this.nbLettresRestantes) ;

        System.out.println("Pourcentage lettres de lettres trouvees : " + partie.getTrouve() + " %");
        
       
        for(Letter l : this.lettres)
        {
            env.removeObject(l);
        }
        
        int l = this.lettres.size();
        for(int i = 0; i < l; i++)
        {
            lettres.remove(0);
        }
        
    }

    
    
    private boolean tuxTrouveLettre() {
        boolean trouve = false;
        
        if( collision(this.lettres.get(0)) ){
            env.removeObject(lettres.get(0));
            lettres.remove(0);
            
            trouve = true;
        }

        return trouve ;
        
    }


    
    
}
