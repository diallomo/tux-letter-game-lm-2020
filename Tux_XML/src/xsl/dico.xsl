<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : dico.xsl
    Created on : 8 octobre 2019, 10:39
    Author     : mezianef
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:dic="http://myGame/tux">
    <xsl:output method="html"/>
    
    <!-- Template principale -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Dictionnaire</title>
            </head>
            
            <body>
                <h1>Dictionnaire</h1>
                <xsl:apply-templates select="//dic:mot">
                <!-- Pour trier les mots dans l'ordre alphabétique -->
                <xsl:sort select="text()" />    
                </xsl:apply-templates>
            </body>
            
        </html>
    </xsl:template>
    
    <!-- Template Mots -->
    <xsl:template match="dic:mot">
        <xsl:value-of select="text()" />
        <br/>
    </xsl:template>
    

</xsl:stylesheet>
